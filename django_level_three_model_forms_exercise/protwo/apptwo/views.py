from django.shortcuts import render
from django.http import HttpResponse
from apptwo.forms import new_user_form

# Create your views here.
def index(request):
    return HttpResponse("<em>My Second App</em>")

def users(request):
    form = new_user_form()
    if request.method == 'POST':
        form = new_user_form(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)

    return render(request, 'apptwo/newuser.html', {'form': form})
