import os
# Configure settings for project
# Need to run this before calling models from application!
os.environ.setdefault('DJANGO_SETTINGS_MODULE','protwo.settings')

import django
# Import settings
django.setup()

import random
from apptwo.models import User
from faker import Faker

fakegen = Faker()

def populate(n):
    '''
    Create N Entries of Dates Accessed
    '''

    for entry in range(n):

        # Create Fake Data for entry
        fake_first_name = fakegen.first_name()
        fake_last_name = fakegen.last_name()
        fake_email = fakegen.email()

        # Create new User Entry
        fake_user = User.objects.get_or_create(first_name=fake_first_name, last_name=fake_last_name, email=fake_email)[0]


if __name__ == '__main__':
    print("Populating the databases...Please Wait")
    populate(20)
    print('Populating Complete')
