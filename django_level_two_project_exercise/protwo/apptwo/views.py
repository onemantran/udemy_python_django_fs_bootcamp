from django.shortcuts import render
from django.http import HttpResponse
from apptwo.models import User

# Create your views here.
def index(request):
    return HttpResponse("<em>My Second App</em>")

def users(request):
    users = {"fake_users":User.objects.order_by('last_name')}
    return render(request, 'apptwo/users.html', context=users)
