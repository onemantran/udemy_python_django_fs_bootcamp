import random
digits = list(range(10))
random.shuffle(digits)
print(digits[:3]) #print to console for debugging purposes
final_three = digits[:3]

print ("Welcome to Code Breaker! Let's see if you can guess my 3 digit number! \nCode has been generated, please guess a 3 digit number")
def run_input():
    guess = input("What is your guess? ")
    guess_str = [int(i) for i in str(guess)]
    check_num(final_three, guess_str)

def check_num(x, y):
    if x[0] == y[0] and x[1] == y[1] and x[2] == y[2]:
        print ("Here is the result of your gues: \nMatch")
    elif x[0] == y[0] or x[1] == y[1] or x[2] == y[2]:
        print ("Here is the result of your gues: \nClose")
        run_input()
    else:
        print ("Here is the result of your gues: \nNope")
        run_input()

run_input()
