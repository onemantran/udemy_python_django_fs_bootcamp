var letter = document.getElementsByTagName("td");
var reset = document.getElementById("rbutton");

function placeLetter() {
  if (this.textContent == "") {
    this.textContent = "X";
  }
  else if (this.textContent == "X") {
    this.textContent = "O";
  }
  else {
    this.textContent = "";
  }
}

function clearBoard() {
  for (var i = 0; i < letter.length; i++) {
    letter[i].textContent = "";
  }
}

for (var i = 0; i < letter.length; i++) {
  letter[i].addEventListener("click", placeLetter);
}

reset.addEventListener("click", clearBoard)
